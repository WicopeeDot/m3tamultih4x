# m3tamultih4x

m3tamultih4x is a fake Garry's Mod cheat in Lua + Python script to "obfuscate" it.

## Usage

m3tamultih4x will run out of the box, no obfuscation required, but if you want to give it to skids you should follow these instructions.

- Install dependencies

```bash
pip install rc4py
npm i -g luamin
```

- Edit the first line of m3tamultih4x.lua and set crashes to true (this will let the annoyances (cheevo, dog, byebye) run)
- Run the Python script

```bash
python3 luafuscate.py m3tamultih4x.lua
```

The last line of output will be the codeblock you send to targets (should start with lua_run_cl). If you'd like them to run it another method (like with the lm command, or in the Lua tab), you can just remove it and prefix accordingly.

Finally, you'll need to social engineer your target into running the script (quick example: "hey, i see you're getting kind of bullied, want my admin script? you can ban people with it")

## Features

- Combat
  - Aimbot: Looks to the opposite direction where the target is.
  - Triggerbot: Runs +attack in console.
  - Anti-Aim: Shakes your view model.
  - Backtracking: Limits your FPS.
- Visuals
  - ESP: Actual working ESP, but only draws one player per frame.
  - Tracers: Draws line from middle of the screen to the bottom of the screen.
  - Chams: Actual working chams, but picks a random player every 3 seconds.
  - Third Person: Places your camera at a random player every 3 seconds.
  - Freecam: Stops sound from playing and puts your camera at worldorigin.
- Exploits
  - Say As Player: Says message yourself.
  - Kick Player: Posts an image of Rover from Windows XP in chat, and covers your screen with said image for 15 seconds.
  - Give Money: Sends all your money to a random player.
  - PAC Stealer: Announces you attempted to steal someone's PAC with a fake PAC stealer in chat.
  - Chat Spammer: Says the tagline for smartphOWNED.com and adds a concommand which bans you for 6 hours. (untested)
  - IP Grabber: Posts selected player's "IP" generated from their EntIndex and last 3 digits of their SteamID64 in chat. (192.168.EntIndex.SteamID64)
  - Server DDOS: Says "!leave DDOS" in chat, causing you to disconnect for reason "DDOS".
  - Server Lua Backdoor: Runs Lua on yourself.
  - Force Mic On: Forces *your* mic on with +voicerecord.
  - Disable Anti-Cheat Hooks: Empties the hooks table.

## Ideas for future stuff

- ESP: make so that when for players outside your field of view it randomizes the position to be a different (but plausible) position

## Credits

@NotNite - Original idea, Combat & Visuals & Exploits actions

@Earthnuker - Python script, Visuals actions, ideas

@Cynosphere - UI design, Combat & Visuals actions

@BriannaFoxwell - Ideas
